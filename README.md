# Movie API

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://movie-api-dedwinnis.herokuapp.com/swagger-ui/index.html)
## Description

Movie API is a RESTful API with Franchises, Movies and Characters with relations to each other.
The API is built with Spring Boot and Hibernate and uses an instance of Keycloak to authenticate calls to the movies endpoint.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)


## Prerequisites

To host the application locally you need to have [Docker Desktop](https://www.docker.com/products/docker-desktop) installed. This can be done by following the instructions for [deploying postgres](#deploying-postgres) using Docker.

## Install

Gradle will install any necessary dependencies.

### Deploying Postgres

This application is using a Postgres for storage, which is deployed using Docker.
The application comes with a docker-compose.yml file which can be used to deploy this and other parts of the application.

Before it is run, a ```.env``` file must be created with the variables specified:
- ```POSTGRES_DB``` - the name of teh database
- ```POSTGRES_PASSWORD``` - the only environment variable that is strictly required by the Postgres image.

Once set up you can then run the following command:
```
docker-compose up -d postgres
```

### Deploying Keycloak

Similarly to Postgres, the application uses Keycloak for authentication.
This can be hosted locally similarly to Postgres, where you specify two environment variables in the ```.env``` file:
- ```KEYCLOAK_USER``` - Admin username &
- ```KEYCLOAK_PASSWORD``` - Admin password to edit the Keycloak instance.

## Usage

For Linux/macOS users, open a terminal and run:

```
./gradlew bootRun
```

For Windows users, use ```gradlew.bat``` instead of ```gradlew``` in PowerShell.

## API

To see all API endpoints, navigate to ```/swagger-ui/index.html``` or click the Heroku badge on the top of the README.

### Update Characters in Movie or Movies in Franchise

To update any of the list relations between entities, you need to provide a list with objects containing ids of the specified entities.
These ids have to refer to already existing entities. 

This can be done like the following:

```json
[
  {
    "id": 1
  },
  {
    "id": 2
  }
]
```
OR: (movie example)

```json
{
  "title": "Captain America: The Winter Soldier",
  "characters": [
    {
      "id": 1
    },
    {
      "id": 2
    }
  ]
}
```


## Maintainers

Dennis Massoumnataj [@DMasso](https://gitlab.com/DMasso), Edwin Eliasson [@edwineliasson98](https://gitlab.com/edwineliasson98)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Dennis Massoumnataj, Edwin Eliasson
