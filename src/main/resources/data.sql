-- Marvel Movie Franchise
INSERT INTO franchise (name, description) VALUES ('MCU', 'Marvel Cinematic Universe');

-- Avengers Endgame
insert into movie (title, genre, director, release_year, picture,trailer, franchise_id)
values ('Avengers: Endgame', 'Action, Adventure, Superhero, Science fiction, Fantasy', 'Anthony Russo, Joe Russo', 2019,
'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQA_-tL18_rj9zEcjN6n41NEaJm-kRNF9UeOtvksZ4z_OW6jRA9',
'https://www.youtube.com/watch?v=TcMBFSGVi1c', 1);

-- insert into movie_franchise (franchise_id, movie_id) values (1,1);

-- Spider man homecoming
insert into movie (director, genre, title, picture, release_year, trailer, franchise_id)
values ('Jon Watts', 'Action, Superhero, Comedy, Science fiction, Adventure, Fantasy, Teen',
'Spider-Man: Homecoming', 'https://upload.wikimedia.org/wikipedia/en/2/24/Spider-Man_Homecoming_soundtrack_cover.jpg',
2017, 'https://www.youtube.com/watch?v=rk-dF1lIbIg', 1);

-- insert into movie_franchise (franchise_id, movie_id)
-- values (1,2);

-- iron man
insert into character (alias, full_name, gender, picture)
values ('Iron Man', 'Tony Stark', 'Male',
        'https://heroichollywood.com/wp-content/uploads/2019/04/Avengers-Robert-Downey-Jr.-Iron-Man-1280x720.jpg');

insert into movie_character (movies_id, characters_id)
values (1,1);
insert into movie_character (movies_id, characters_id)
values (2,1);

-- Captain America
insert into character (alias, full_name, gender, picture)
values ('Captain America', 'Steve Rogers', 'Male',
        'https://new.static.tv.nu/92246470?forceFit=0&height=720&quality=50&width=1280');
insert into movie_character (movies_id, characters_id)
values (1,2);

-- Spider-Man
insert into character (alias, full_name, gender, picture)
values ('Spider-Man', 'Peter Parker', 'Male', 'https://cdn.pocket-lint.com/r/s/1200x630/assets/images/159248-tv-news-sony-confirms-tom-holland-will-play-spider-man-in-new-trilogy-of-mcu-movies-image1-wxjxzwtlmc.jpg');

insert into movie_character (movies_id, characters_id)
values (1,3);
insert into movie_character (movies_id, characters_id)
values (2,3);

-- DC Movie franchise

INSERT INTO franchise (name, description) VALUES ('DCEU', 'DC Extended Universe');

-- The Batman
insert into movie (director, genre, title, picture, release_year, trailer, franchise_id)
values ('Matt Reeves', 'Action, Superhero, Superhero, Adventure, Drama, Crime film, Mystery','The Batman',
        'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRBkxgl2A2PhE_6tklFLT0bxn5NLhvhsnpXGhmXBt_zotrlVHmo',
        2022,'https://www.youtube.com/watch?v=mqqft2x_Aa4', 2);

-- insert into movie_franchise (franchise_id, movie_id)
-- values (2,3);

-- Batman
insert into character (alias, full_name, gender, picture)
values ('Batman','Bruce Wayne','Male','https://medialeaflets.com/wp-content/uploads/2022/02/Robert-Pattinson-wants-to-star-in-a-Batman-trilogy-says.jpg');

insert into movie_character (movies_id, characters_id)
values (3,4);

-- Catwoman
insert into character (alias, full_name, gender, picture)
values ('Catwoman','Selina Kyle','Female','https://static0.cbrimages.com/wordpress/wp-content/uploads/2021/12/Zoe-Kravitz-standing-as-Catwoman-in-The-Batman.jpg');

insert into movie_character (movies_id, characters_id)
values (3,5);

-- Justice League
insert into movie (director, genre, title, picture, release_year, trailer, franchise_id)
values ('Zack Snyder', 'Action, Science fiction, Adventure, Fantasy, Superhero', 'Zack Snyder''s Justice League',
        'https://m.media-amazon.com/images/M/MV5BYjI3NDg0ZTEtMDEwYS00YWMyLThjYjktMTNlM2NmYjc1OGRiXkEyXkFqcGdeQXVyMTEyMjM2NDc2._V1_FMjpg_UX1000_.jpg',
        2021, 'https://www.youtube.com/watch?v=vM-Bja2Gy04', 2);

-- insert into movie_franchise (franchise_id, movie_id)
--     values (2,4);

-- Aquaman
insert into character (alias, full_name, gender, picture)
values ('Aquaman','Arthur Curry','Male','https://www.denofgeek.com/wp-content/uploads/2021/08/aquaman-2-jason-momoa.jpg?fit=2000%2C1125');

insert into movie_character (movies_id, characters_id)
values (4,6);

-- Wonder Woman
insert into character (alias, full_name, gender, picture)
values ('Wonder Woman','Diana Prince','Female',
        'https://www.filmtopp.se/media/2017/05/Wonder-Woman-Movie-Artwork.jpeg?w=810&fit=max&s=1f64fd210cd8fe8ef6cd606ca7d1786a');

insert into movie_character (movies_id, characters_id)
values (4,7);