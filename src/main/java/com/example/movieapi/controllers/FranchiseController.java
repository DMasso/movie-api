package com.example.movieapi.controllers;


import com.example.movieapi.models.CommonResponse;
import com.example.movieapi.models.dbo.Character;
import com.example.movieapi.models.dbo.Franchise;
import com.example.movieapi.models.dbo.Movie;
import com.example.movieapi.repositories.FranchiseRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@Tag(name = "Franchise")
@CrossOrigin(origins = "*")
@RequestMapping("/api/v0/franchises")
public class FranchiseController {

    private final FranchiseRepository franchiseRepository;

    public FranchiseController(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    @GetMapping
    @Operation(summary = "Get all franchises")
    public ResponseEntity<CommonResponse<List<Franchise>>> getAllFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(franchises));
    }

    @GetMapping("{id}")
    @Operation(summary = "Get franchise by id")
    public ResponseEntity<CommonResponse<Franchise>> getFranchiseById(@PathVariable Long id) {
        Optional<Franchise> franchise = franchiseRepository.findById(id);
        if (franchise.isPresent()) {
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse<>(franchise.get()));
        }

        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(406, "Franchise doesn't exist"));
    }

    @PostMapping
    @Operation(summary = "Create franchise")
    public ResponseEntity<CommonResponse<Franchise>> createFranchise(@RequestBody Franchise franchise) {
        Franchise savedFranchise = franchiseRepository.save(franchise);

        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(savedFranchise));
    }

    @PutMapping("{id}")
    @Operation(summary = "Update franchise")
    public ResponseEntity<CommonResponse<Franchise>> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise) {
        Franchise returnFranchise = new Franchise();

        if(!id.equals(franchise.id)){
            return ResponseEntity
                    .badRequest()
                    .body(new CommonResponse<>(409, "Not matching ids"));
        }
        returnFranchise = franchiseRepository.save(franchise);
        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(returnFranchise));
    }

    @PutMapping("{id}/movies")
    @Operation(summary = "Update movies in franchise")
    public ResponseEntity<CommonResponse<Franchise>> updateMoviesInFranchise(@PathVariable Long id, @RequestBody List<Movie> movies) {
        Optional<Franchise> franchise = franchiseRepository.findById(id);
        if (franchise.isPresent()) {
            franchise.get().movies = movies;
            Franchise savedFranchise = franchiseRepository.save(franchise.get());
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse<>(savedFranchise));
        }

        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(406, "Franchise doesn't exist"));
    }

    @GetMapping("{id}/movies")
    @Operation(summary = "Get movies in franchise")
    public ResponseEntity<CommonResponse<List<Movie>>> getMoviesInFranchise(@PathVariable Long id) {
        Optional<Franchise> franchise = franchiseRepository.findById(id);
        if (franchise.isPresent()){
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse<>(franchise.get().movies));
        }

        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(406, "Franchise doesn't exist"));
    }

    @GetMapping("{id}/characters")
    @Operation(summary = "Get characters in franchise")
    public ResponseEntity<CommonResponse<Collection<Character>>> getCharactersInFranchise(@PathVariable Long id) {
        List<Character> characters = franchiseRepository.findCharactersInFranchise(id);
        if(!characters.isEmpty()) {
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse<>(characters));
        }
        return ResponseEntity
                .status(406)
                .body(new CommonResponse<>(406, "Franchise doesn't exist or doesn't have any movies"));
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Delete franchise")
    public ResponseEntity<CommonResponse<Franchise>> deleteFranchise(@PathVariable Long id) {
        Optional<Franchise> franchiseToDelete = franchiseRepository.findById(id);
        if (franchiseToDelete.isPresent()) {
            // remove reference to deleted movie from all characters
            franchiseToDelete.get().movies.forEach(movie -> {
                movie.franchise = null;
            });
            franchiseRepository.delete(franchiseToDelete.get());
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse<>(franchiseToDelete.get()));
        }
        return ResponseEntity
                .badRequest()
                .body(new CommonResponse<>(400, "Franchise doesn't exist"));
    }

}
