package com.example.movieapi.controllers;

import com.example.movieapi.models.CommonResponse;
import com.example.movieapi.models.dbo.Character;
import com.example.movieapi.models.dbo.Movie;
import com.example.movieapi.repositories.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Movie")
@CrossOrigin(origins = "*")
@SecurityRequirement(name = "keycloak_implicit")
@RequestMapping("/api/v0/movies")
public class MovieController {
    private final MovieRepository movieRepository;

    public MovieController(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }
    @GetMapping
    @Operation(summary = "Get all movies")
    public ResponseEntity<CommonResponse<List<Movie>>> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();
        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(movies));
    }

    @GetMapping("{id}")
    @Operation(summary = "Get movie by id")
    public ResponseEntity<CommonResponse<Movie>> getMovieById(@PathVariable Long id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent()) {
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse<>(movie.get()));
        }

        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(406, "Movie doesn't exist"));
    }

    @PostMapping
    @Operation(summary = "Create movie")
    public ResponseEntity<CommonResponse<Movie>> createMovie(@RequestBody Movie movie) {
        Movie savedMovie = movieRepository.save(movie);

        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(savedMovie));
    }

    @PutMapping("{id}")
    @Operation(summary = "Update movie")
    public ResponseEntity<CommonResponse<Movie>> updateMovie(@PathVariable Long id, @RequestBody Movie movie) {
        Movie returnMovie = new Movie();

        if(!id.equals(movie.id)){
            return ResponseEntity
                    .badRequest()
                    .body(new CommonResponse<>(409, "Not matching ids"));
        }
        returnMovie = movieRepository.save(movie);
        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(returnMovie));
    }

    @PutMapping("{id}/characters")
    @Operation(summary = "Update characters in movie")
    public ResponseEntity<CommonResponse<Movie>> updateCharactersInMovie(@PathVariable Long id, @RequestBody List<Character> characters) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent()) {
            movie.get().characters = characters;
            Movie savedMovie = movieRepository.save(movie.get());
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse<>(savedMovie));
        }

        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(406, "Movie doesn't exist"));
    }

    @GetMapping("{id}/characters")
    @Operation(summary = "Get characters in movie")
    public ResponseEntity<CommonResponse<List<Character>>> getCharactersInMovie(@PathVariable Long id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent()){
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse<>(movie.get().characters));
        }

        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(406, "Movie doesn't exist"));
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Delete movie")
    public ResponseEntity<CommonResponse<Movie>> deleteMovie(@PathVariable Long id) {
        Optional<Movie> movieToDelete = movieRepository.findById(id);
        if (movieToDelete.isPresent()) {
            // remove reference to deleted movie from all characters
            movieToDelete.get().characters.forEach(character -> {
                character.movies.remove(movieToDelete.get());
            });
            movieRepository.delete(movieToDelete.get());
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse<>(movieToDelete.get()));
        }
        return ResponseEntity
                .badRequest()
                .body(new CommonResponse<>(400, "Movie doesn't exist"));
    }

}
