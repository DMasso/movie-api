package com.example.movieapi.controllers;

import com.example.movieapi.models.CommonResponse;
import com.example.movieapi.models.dbo.Character;
import com.example.movieapi.repositories.CharacterRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@Tag(name = "Character")
@CrossOrigin(origins = "*")
@RequestMapping("/api/v0/characters")
public class CharacterController {


    private final CharacterRepository characterRepository;

    public CharacterController(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @GetMapping
    @Operation(summary = "Get all characters")
    public ResponseEntity<CommonResponse<List<Character>>> getAllCharacters() {
        List<Character> characters = characterRepository.findAll();
        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(characters));
    }

    @GetMapping("{id}")
    @Operation(summary = "Get character by id")
    public ResponseEntity<CommonResponse<Character>> getCharacterById(@PathVariable Long id) {
        Optional<Character> character = characterRepository.findById(id);
        if (character.isPresent()) {
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse<>(character.get()));
        }

        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(406, "Character doesn't exist"));
    }

    @PostMapping
    @Operation(summary = "Create character")
    public ResponseEntity<CommonResponse<Character>> createCharacter(@RequestBody Character character) {
        Character savedCharacter = characterRepository.save(character);

        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(savedCharacter));
    }

    @PutMapping("{id}")
    @Operation(summary = "Update character")
    public ResponseEntity<CommonResponse<Character>> updateCharacter(@PathVariable Long id, @RequestBody Character character) {
        Character returnCharacter = new Character();

        if(!id.equals(character.id)){
            return ResponseEntity
                    .badRequest()
                    .body(new CommonResponse<>(409, "Not matching ids"));
        }
        returnCharacter = characterRepository.save(character);
        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(returnCharacter));
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Delete character")
    public ResponseEntity<CommonResponse<Character>> deleteCharacter(@PathVariable Long id) {
        Optional<Character> characterToDelete = characterRepository.findById(id);
        if (characterToDelete.isPresent()) {
            // remove reference to deleted movie from all characters
            characterToDelete.get().movies.forEach(movie -> {
                movie.characters.remove(characterToDelete.get());
            });
            characterRepository.delete(characterToDelete.get());
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse<>(characterToDelete.get()));
        }
        return ResponseEntity
                .badRequest()
                .body(new CommonResponse<>(400, "Character doesn't exist"));
    }

}
