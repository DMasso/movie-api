package com.example.movieapi.repositories;

import com.example.movieapi.models.dbo.Character;
import com.example.movieapi.models.dbo.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
    @Query("""
        select c from Movie m join m.characters c on c.id = (select id from Character where c.id = id)
        where m.franchise.id = ?1
        group by c.id
    """)
    List<Character> findCharactersInFranchise(Long id);
}
