package com.example.movieapi.models.dbo;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @Column(nullable = false, length = 50)
    public String title;

    @Column(nullable = false, length = 100)
    public String genre;

    @Column(nullable = false)
    public int releaseYear;

    @Column(nullable = false, length = 50)
    public String director;

    @Column
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String picture;

    @Column(nullable = false, length = 100)
    public String trailer;

    @JsonGetter("franchise")
    public String franchise(){
        if (franchise != null) {
            return "/api/v0/franchises/" + franchise.id;
        }
        return null;
    }

    @ManyToOne()
    @JoinColumn(name = "franchise_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Franchise franchise;

    @JsonGetter("characters")
    public List<String> characters() {
        if(characters != null) {
            return characters.stream()
                    .map(character -> {
                        return "/api/v0/characters/" + character.id;
                    }).collect(Collectors.toList());
        }
        return null;
    }
    @ManyToMany
    @JoinTable(name = "movie_character")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<Character> characters;

}
