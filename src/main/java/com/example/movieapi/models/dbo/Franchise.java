package com.example.movieapi.models.dbo;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @Column(nullable = false, length = 20)
    public String name;

    @Column(length = 100)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String description;

    @JsonGetter("movies")
    public List<String> movies() {
        if (movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "/api/v0/movies/" + movie.id;
                    }).collect(Collectors.toList());
        }
        return null;
    }
    @OneToMany(mappedBy = "franchise", fetch = FetchType.LAZY)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<Movie> movies = new ArrayList<>();
}
