package com.example.movieapi.models.dbo;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @Column(nullable = false, length = 50)
    public String fullName;

    @Column(nullable = false, length = 50)
    public String alias;

    @Column(nullable = false, length = 10)
    public String gender;

    @Column
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String picture;

    @JsonGetter("movies")
    public List<String> movies() {
        if (movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "/api/v0/movies/" + movie.id;
                    }).collect(Collectors.toList());
        }
        return null;
    }

    @ManyToMany(mappedBy = "characters", fetch = FetchType.LAZY)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<Movie> movies;

}
